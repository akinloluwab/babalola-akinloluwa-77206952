﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Babalola_Akinloluwa_Samuel___77206952
{
    class Circle : Shapes
    {
        /// <summary>
        /// receives parameters (int x, int y) and draws a circle
        /// </summary>
        /// <param name="pictureBox">draws a circle</param>
        public void drawShape(PictureBox pictureBox, int x1, int y1, int x4, Bitmap bitmap)
        {
            Pen p = new Pen(Color.Black, 2);
            Graphics graphics = Graphics.FromImage(bitmap);
            graphics.DrawEllipse(p, x1, y1, x4, x4);
            pictureBox.Image = bitmap;
            p.Dispose();
        }
    }
}
