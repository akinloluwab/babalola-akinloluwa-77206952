﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Babalola_Akinloluwa_Samuel___77206952
{
    class Rectangle : Shapes
    {
        /// <summary>
        /// receives parameters (int x, int y) and draws a rectangle
        /// </summary>
        /// <param name="pictureBox">draw a rectangle</param>
        public void drawShape(PictureBox pictureBox, int x1, int y1, int x3, int y3, Bitmap bitmap)
        {
            Pen p = new Pen(Color.Black, 2);
            Graphics graphics = Graphics.FromImage(bitmap);
            graphics.DrawRectangle(p, x1, y1, x3, y3);
            pictureBox.Image = bitmap;
            p.Dispose();
        }
    }
}
