﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Babalola_Akinloluwa_Samuel___77206952
{
    public partial class Form1 : Form
    {
       
        private TextCommands textCommands = new TextCommands();
        /// <summary>
        /// initializes component
        /// </summary>
        public Form1()
        {
            InitializeComponent();
        }

        public void textBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }
        
        /// <summary>
        /// runs command when enter key is pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox_KeyUp_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Enter))
            {
                
                if (textBox.Text.Equals("Run", StringComparison.OrdinalIgnoreCase))
                {
                    textCommands.Multiline(richTextBox, textBox, pictureBox);
                }
                else
                {
                    textCommands.initMyVariables();
                    textCommands.TextCommand(textBox, pictureBox, richTextBox, textBox.Text);
                }
            }  
        }


        private void Form1_Paint(object sender, PaintEventArgs e)
        {
        }

        public void pictureBox_Paint(object sender, PaintEventArgs e)
        {  
        }

        private void richTextBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void button_Paint(object sender, PaintEventArgs e)
        {  
        }

        /// <summary>
        /// loads a program
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            Load l = new Load();
            l.load(richTextBox);
        }

        /// <summary>
        /// saves a program
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            Save s = new Save();
            s.save(richTextBox);
        }
    }
}
