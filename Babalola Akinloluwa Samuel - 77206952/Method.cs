﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows.Forms;

namespace Babalola_Akinloluwa_Samuel___77206952
{
    public class Method
    {
        public string methodName;
        public ArrayList methodLines;
        public bool methodFlag;
        public OrderedDictionary paramMap;
        public Method()
        {
            methodName = "";
            methodLines = new ArrayList();
            methodFlag = false;
            paramMap = new OrderedDictionary();
        }
        public Method(string methodname, string[] param_list)
        {
            methodName = methodname;
            methodLines = new ArrayList();
            methodFlag = true;
            paramMap = new OrderedDictionary();
            foreach (string param in param_list)
            {
                if (param.Length == 0)
                {
                    continue;
                }
                
                if (paramMap.Contains(param))
                {
                    MessageBox.Show(string.Format("{0} argument name already used/declared", param));
                }
                else
                {
                    paramMap.Add(param, 0);
                }
                
            }
        }
        public bool assignParameterValues(ArrayList param_list)
        {
            if (param_list.Count != paramMap.Count)
            {
                return false;
            }
            for (int k = 0; k < param_list.Count; k++)
            {
                paramMap[k] = param_list[k];
            }
            return true;
        }
    }
}

