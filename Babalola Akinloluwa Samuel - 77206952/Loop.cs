﻿using System.Collections;

namespace Babalola_Akinloluwa_Samuel___77206952
{
    public class Loop
    {
        /// <summary>
        /// Variable count of int data type
        /// </summary>
        public int count;

        /// <summary>
        /// ArrayList loopLines declaration
        /// </summary>
        public ArrayList loopLines;

        /// <summary>
        /// Flag variable
        /// </summary>
        public bool loopFlag;

        /// <summary>
        /// Loop Constructor assigns initial values to variables count and loopFlag
        /// </summary>
        public Loop()
        {
            count = 0;
            loopFlag = false;
            loopLines = new ArrayList();
        }

        /// <summary>
        /// Loop constructor declares integer loopCount, assigns final values to variables count and loopFlag
        /// </summary>
        /// <param name="loopCount"></param>
        public Loop(int loopCount)
        {
            count = loopCount;
            loopLines = new ArrayList();
            loopFlag = true;
        }

    }

}
