﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Babalola_Akinloluwa_Samuel___77206952
{
    class Triangle : Shapes
    {
        /// <summary>
        /// receives parameters (side, side, side) and draws a triangle
        /// </summary>
        /// <param name="pictureBox">draws a triangle</param>
        public void drawShape(PictureBox pictureBox, double a, double b, double c, int x1, int y1, Bitmap bitmap)
        {
            Pen p = new Pen(Color.Black, 2);
            double cosABC = (Math.Pow(a, 2) + Math.Pow(c, 2) - Math.Pow(b, 2)) / (2 * a * c);
            double angleB = Math.Acos(cosABC);
            Point C = new Point((int)Math.Round(a * Math.Cos(angleB)) + x1, y1);
            Point B = new Point(x1, (int)Math.Round(a * Math.Sin(angleB)) + y1);
            Point A = new Point((int)Math.Round(x1 + c), (int)Math.Round(a * Math.Sin(angleB)) + y1);
            Graphics graphics = Graphics.FromImage(bitmap);
            Point[] points = { A, B, C };
            graphics.DrawPolygon(p, points);
            pictureBox.Image = bitmap;
            p.Dispose();
        }
    }
}
