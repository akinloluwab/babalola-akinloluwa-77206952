﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Specialized;
using System.Collections;

namespace Babalola_Akinloluwa_Samuel___77206952
{
    /// <summary>
    /// value type defined by set of commands
    /// </summary>
    public enum Flags { IF, ENDIF, METHOD, ENDMETHOD, LOOP, ENDLOOP, CALL, DEFAULT};

    public class TextCommands
    {
        private DrawFunction drawFunction = new DrawFunction();
        private OrderedDictionary variablesMap = new OrderedDictionary();
        private Loop loopObject = new Loop(); //handle loops

        //handle if block
        private bool executeIFBlock = false;
        private bool startIFBlock = false;
        
        //for handling methods
        //private Method methodObject = new Method();
        private Dictionary<string, Method> methodsMap = new Dictionary<string, Method>();
        private string createMethod = "";
        private bool methodCallFlag = false;//changes to true during method call
        private OrderedDictionary myVariables;//decide which variable to use
        /// <summary>
        /// receives various drawing commands 
        /// </summary>
        /// <param name="textBox">run commands with a textbox</param>
        /// <param name="pictureBox">output commands on a PictureBox</param>
        /// <param name="richTextBox">receives multiple line commands and runs them with a "run" command in the textBox</param>
        /// <param name="line">Text in every line in the richTextBox</param>
        public void TextCommand(TextBox textBox, PictureBox pictureBox, RichTextBox richTextBox, string line)
        {
            string[] command = line.Split(' '); //basic drawing commands(moveto, drawto, circle, etc.)
            string caseInsensitivity = command[0].ToLower(); //commands are case insensitive
            

            if (caseInsensitivity == "clear")
            {
                drawFunction.clear(pictureBox);   
            }
            else if (caseInsensitivity == "")
            {
                MessageBox.Show("Please input a command");
            }
            else if (caseInsensitivity == "reset")
            {
                drawFunction.reset();
                variablesMap.Clear();
                methodsMap.Clear();
                methodCallFlag = false;
                myVariables.Clear();
            }
            else if ((caseInsensitivity == "drawto") || (caseInsensitivity == "moveto"))
            {
                if (command.Length == 2)
                {
                    string parameters = command[1]; //receives parameters from commands((50,60) (60,45))
                    string[] parameterSplit = parameters.Split(','); //splits parameters into arrays
                    if (parameterSplit.Length == 2)
                    {
                        string getSplitParameter1 = parameterSplit[0];
                        string getSplitParameter2 = parameterSplit[1];

                        try
                        {
                            if (caseInsensitivity == "moveto")
                            {
                                drawFunction.SetCoordinates(checkVariable(getSplitParameter1,myVariables), checkVariable(getSplitParameter2,myVariables));
                            }
                            else if (caseInsensitivity == "drawto")
                            {
                                drawFunction.DrawCoordinates(checkVariable(getSplitParameter1, myVariables), checkVariable(getSplitParameter2, myVariables));

                                drawFunction.drawTo(pictureBox);
                            }
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Please input an integer!"); //if integer values not entered (a,b)
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Please input appropriate commands!"); //if commands asides moveto or drawto is received
                }
            }
            else if ((caseInsensitivity == "rectangle") || (caseInsensitivity == "circle") || (caseInsensitivity == "triangle"))
            {
                if (command.Length == 2)
                {
                    String coordinates = command[1]; //(60,50)
                    String[] coordinateSplit = coordinates.Split(',');

                    if (caseInsensitivity == "rectangle")
                    {

                        if (coordinateSplit.Length == 2)
                        {
                            String getWidth = coordinateSplit[0];
                            String getHeight = coordinateSplit[1];
                            int width = checkVariable(getWidth, myVariables);
                            int height = checkVariable(getHeight, myVariables);
                            
                            drawFunction.X3(width);
                            drawFunction.Y3(height);
                            drawFunction.drawRectangle(pictureBox);
                        }
                    }
                    else if (caseInsensitivity == "circle")
                    {
                        if (coordinateSplit.Length == 1)
                        {
                            String getRadius = coordinateSplit[0];
                            int radius = checkVariable(getRadius, myVariables);
                           
                            drawFunction.X4(radius);
                            drawFunction.drawCircle(pictureBox);
                        }
                    }
                    else if (caseInsensitivity == "triangle")
                    {
                        if (coordinateSplit.Length == 3)
                        {
                            String getSide1 = coordinateSplit[0];
                            String getSide2 = coordinateSplit[1];
                            String getSide3 = coordinateSplit[2];
                            double side1 = checkVariable(getSide1, myVariables);
                            double side2 = checkVariable(getSide2, myVariables);
                            double side3 = checkVariable(getSide3, myVariables);
                            drawFunction.A(side1);
                            drawFunction.B(side2);
                            drawFunction.C(side3);
                            drawFunction.drawTriangle(pictureBox);

                        }
                    }
                }
                else
                {
                    MessageBox.Show("This command is without a parameter. Please input a parameter");
                }
            }
            else if (caseInsensitivity == "var")
            {
                string variableName = command[1]; //dictionary key
                int variableValue = 0; //dictionary value
                if ((command.Length == 4) && (command[2] == "=")) //var abc = 20
                {
                    variableValue = Int32.Parse(command[3]); //assigns 20 to the variable abc
                }
                else if (command.Length != 2)
                {
                    MessageBox.Show("Syntax Error!");
                }
                try
                {
                    myVariables.Add(variableName, variableValue); // assigns value to corresponding key in dictionary variableMap
                }
                catch (ArgumentException)
                {
                    MessageBox.Show("Syntax Error! Variable name already declared");
                }
            }
            else if (command[1] == "=") //abc = 10
            {

                myVariables[command[0]] = checkVariable(command[2],myVariables); //assigns the value of 10 to corresponding key in dictionary

            }
            else if (command[1] == "+")
            {
                int lhs = checkVariable(command[0], myVariables);
                int rhs = checkVariable(command[2], myVariables);
                myVariables[command[0]] = lhs + rhs;
            }
            else if (command[1] == "*")
            {
                int lhs = checkVariable(command[0],myVariables);
                int rhs = checkVariable(command[2],myVariables);

                if ((lhs * rhs) < 600)
                {
                    myVariables[command[0]] = lhs * rhs;
                }
                else
                {
                    MessageBox.Show(string.Format("{0} * {1} results in is too large to fit into drawing area", myVariables[command[0]], rhs));
                }

            }
            else if (command[1] == "/")
            {
                int lhs = checkVariable(command[0], myVariables);
                int rhs = checkVariable(command[2], myVariables);
                myVariables[command[0]] = lhs / rhs;

            }
            else if (command[1] == "-")
            {
                int lhs = checkVariable(command[0], myVariables);
                int rhs = checkVariable(command[2], myVariables);
                variablesMap[command[0]] = lhs - rhs;
            }
                
        }
        /// <summary>
        /// Method that checks if data is a literal value or some earlier declared variable
        /// </summary>
        /// <param name="variable">string variable converted to integer value</param>
        /// <param name="varDictionary">collection of variables declared by the user</param>
        /// <returns></returns>
        public int checkVariable(string variable, OrderedDictionary varDictionary)
        {//
            int v = 0;
            if (varDictionary.Contains(variable))
            {
                v =  (int) varDictionary[variable]; //either methodLocalVariable or variableMap
            }
            else
            {
                try
                {
                    v = Int32.Parse(variable); //receives integer values
                }
                catch (Exception e)
                {
                    MessageBox.Show(string.Format("{0} exception occured. {1} Not declared",e,variable));
                }
            }
            return v;
        }
        public bool executeIFCondition(int data1, string condition, int data2)
        {
            bool comparisonResult = false;
            if (condition == "==")
            {
                comparisonResult = data1 == data2;
            }
            else if (condition == ">")
            {
                comparisonResult = data1 > data2;
            }
            else if (condition == ">=")
            {
                comparisonResult = data1 >= data2;
            }
            else if (condition == "<")
            {
                comparisonResult = data1 < data2;
            }
            else if (condition == "<=")
            {
                comparisonResult = data1 <= data2;
            }
            else
            {
                MessageBox.Show(string.Format("{0} is an invalid comparison symbol",condition));
            }
            return comparisonResult;
        }

        public int ArithmeticOperator(int value1, string operators, int value2)
        {
            int arithmeticResult = 0;
            if (operators == "+")
            {
                arithmeticResult = value1 + value2;
            }
            else if (operators == "-")
            {
                arithmeticResult = value1 - value2;
            }
            else if (operators == "*")
            {
                arithmeticResult = value1 * value2;
            }
            else if (operators == "/")
            {
                arithmeticResult = value1 / value2;
            }
            return arithmeticResult;
        }
        /// <summary>
        /// boolean class to indicate if a condition has been met
        /// </summary>
        /// <param name="line">each line in the richTextBox</param>
        /// <returns></returns>
        public Flags AssignFlag(string line)
        {
            Flags flag = Flags.DEFAULT;
            if (line.StartsWith("loop", StringComparison.CurrentCultureIgnoreCase))
            {
                flag = Flags.LOOP;
            }
            else if (line.StartsWith("endloop", StringComparison.CurrentCultureIgnoreCase))
            {
                flag = Flags.ENDLOOP;
            }
            else if (line.StartsWith("if ", StringComparison.CurrentCultureIgnoreCase))
            {
                flag = Flags.IF;
            }
            else if (line.StartsWith("endif", StringComparison.CurrentCultureIgnoreCase))
            {
                flag = Flags.ENDIF;
            }
            else if (line.StartsWith("method", StringComparison.CurrentCultureIgnoreCase))
            {
                flag = Flags.METHOD;
            }
            else if (line.StartsWith("endmethod", StringComparison.CurrentCultureIgnoreCase))
            {
                flag = Flags.ENDMETHOD;
            }
            else if (line.StartsWith("call", StringComparison.CurrentCultureIgnoreCase))
            {
                flag = Flags.CALL;
            }
            return flag;
        }
        /// <summary>
        /// initializes myVariables dictionary by assigning variablesMap to it
        /// </summary>
        public void initMyVariables()
        {
            myVariables = variablesMap;
        }

        /// <summary>
        /// Method that reads and executes commands on command line one at a time
        /// </summary>
        /// <param name="richTextBox">receives multiline commands</param>
        /// <param name="textBox">receives single line command</param>
        /// <param name="pictureBox">drawing area for the display of the results of commands</param>
        public void Multiline(RichTextBox richTextBox, TextBox textBox, PictureBox pictureBox)
        {
            String[] newLine = { "\r\n", "\n", "\r" };
            String[] multiline = richTextBox.Text.Split(newLine, StringSplitOptions.RemoveEmptyEntries);

            
            Flags flag = Flags.DEFAULT;
            initMyVariables();
            foreach (String line in multiline)
            {
                flag = AssignFlag(line);
                string[] command = line.Split(' ');
                string[] methodCommand;
                switch (flag)
                {
                    case Flags.LOOP:
                        string caseInsensitivity = command[0].ToLower();
                        string midKeyword = command[1].ToLower();
                        if (variablesMap.Contains(command[2]) && midKeyword.Equals("for"))
                        {
                            loopObject = new Loop((int)variablesMap[command[2]]);
                        }
                        else
                        {
                            MessageBox.Show(string.Format("{0} is not declared\n", command[0]));
                        }
                        break;
                    case Flags.ENDLOOP:
                        while (loopObject.count > 0)
                        {
                            foreach (String loopLine in loopObject.loopLines)
                            {
                                TextCommand(textBox, pictureBox, richTextBox, loopLine);
                            }

                            loopObject.count -= 1;
                        }
                        loopObject = new Loop();
                        flag = Flags.DEFAULT;
                        break;
                    case Flags.IF:
                        int data1 = checkVariable(command[1], variablesMap);
                        int data2 = checkVariable(command[3], variablesMap);
                        executeIFBlock = executeIFCondition(data1, command[2], data2);
                        startIFBlock = true;
                        break;
                    case Flags.ENDIF:
                        executeIFBlock = false;
                        startIFBlock = false;
                        flag = Flags.DEFAULT;
                        break;
                    case Flags.METHOD:
                        methodCommand = line.Split('(');
                        command = methodCommand[0].Split(' '); //extract method keyword and methodname

                        if (methodsMap.ContainsKey(command[1]))
                        {
                            MessageBox.Show("Method already declared!!");
                        }
                        else
                        {
                            createMethod = command[1]; //keep name of method to create

                            //process method arguments
                            if (methodCommand[1].Contains(")"))
                            {
                                char[] charactersToTrim = { ' ', ')' };
                                methodCommand[1] = methodCommand[1].Trim(charactersToTrim);

                                methodsMap.Add(createMethod, new Method(command[1], methodCommand[1].Split(',')));
                            }
                            else
                            {
                                MessageBox.Show(string.Format("syntax error. [{0}] is missing ')'", line));
                            }
                        }
                        break;
                    case Flags.ENDMETHOD:
                        createMethod = "";
                        flag = Flags.DEFAULT;
                        break;
                    case Flags.CALL:
                        methodCommand = line.Split('(');
                        command = methodCommand[0].Split(' ');

                        if (methodCommand[1].Contains(")"))
                        {
                            char[] charactersToTrim = { ' ', ')' }; 
                            methodCommand[1] = methodCommand[1].Trim(charactersToTrim);
                            string[] params_list = methodCommand[1].Split(',').Where(x => !string.IsNullOrWhiteSpace(x)).ToArray(); //call c( , )

                            if (methodsMap.ContainsKey(command[1]))
                            {
                                if (params_list.Length == methodsMap[command[1]].paramMap.Count)
                                {
                                    //get values passed to method
                                    ArrayList param_array = new ArrayList();
                                    foreach (string param in params_list)
                                    {
                                        param_array.Add(checkVariable(param, variablesMap));
                                    }
                                    // assign the values of parameters to paramMap in method object
                                    methodCallFlag = methodsMap[command[1]].assignParameterValues(param_array);

                                    if (methodCallFlag)
                                    {
                                        myVariables = methodsMap[command[1]].paramMap;
                                    }
                                    else
                                    {
                                        MessageBox.Show("Failed to assign parameters/arguments of the method");
                                        break;
                                    }
                                    //executes lines in the method
                                    foreach (String methodline in methodsMap[command[1]].methodLines)
                                    {
                                        TextCommand(textBox, pictureBox, richTextBox, methodline);
                                    }
                                    //destroy method local variables
                                    methodCallFlag = false;//method finished executing
                                    myVariables = variablesMap;
                                }
                                else
                                {
                                    MessageBox.Show(string.Format("Syntax error. Expected {0} parameters but got {1} parameters", methodsMap[command[1]].paramMap.Count, params_list.Length));
                                }                                
                            }
                            else
                            {
                                MessageBox.Show(string.Format("{0} method not declared", command[1]));
                            }                    
                        }
                        else
                        {
                            MessageBox.Show(string.Format("Syntax error. [{0}] is missing ')'", line));
                        }
                        flag = Flags.DEFAULT;
                        break;
                    default:
                        if ((executeIFBlock == false) && (startIFBlock == true))
                        {
                            break;
                        }

                        if (methodsMap.ContainsKey(createMethod))
                        {
                            methodsMap[createMethod].methodLines.Add(line);
                            break;
                        }
                        if (loopObject.loopFlag)
                        {
                            loopObject.loopLines.Add(line);
                        }
                        else
                        {
                            TextCommand(textBox, pictureBox, richTextBox, line);
                        }
                        break;
                }    
            }
        }
    }
}
