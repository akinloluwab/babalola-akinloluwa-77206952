﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;



namespace Babalola_Akinloluwa_Samuel___77206952
{
    /// <summary>
    /// class DrawFunction declares methods for the drawing of different shapes.
    /// </summary>
    public class DrawFunction
    {
        private int x3, y3, x4 = 0;
        private double a = 0;
        private double b = 0;
        private double c = 0;
        Pen p = new Pen(Color.Black, 2);

        /// <summary>
        /// x-coordinate for moveto function
        /// </summary>
        public int x1 { get; private set; }

        /// <summary>
        /// y-coordinate for moveto function
        /// </summary>
        public int y1 { get; private set; }

        /// <summary>
        /// x-coordinate for drawto function
        /// </summary>
        public int x2 { get; private set; }

        /// <summary>
        /// y-coordinate for drawto function
        /// </summary>
        public int y2 { get; private set; }

        Bitmap bitmap = new Bitmap(600, 600);

        /// <summary>
        /// constructor for class DrawFunction
        /// </summary>
        public DrawFunction()
        {
            x1 = 0;
            y1 = 0;
            x2 = 0;
            y2 = 0;
        }

        public void A(double a)
        {
            this.a = a;
        }

        public void B(double b)
        {
            this.b = b;
        }

        public void C(double c)
        {
            this.c = c;
        }
       

        /// <summary>
        /// move pen to (x,y) coordinates
        /// </summary>
        /// <param name="x1">moves pen to x position</param>
        /// <param name="y1">moves pen to y position</param>
        public void SetCoordinates(int x1, int y1)
        {
            this.x1 = x1;
            this.y1 = y1;
        }
        
        /// <summary>
        /// inputs integer values for x2 and y2 for the drawto line command
        /// </summary>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        public void DrawCoordinates(int x2, int y2)
        {
            this.x2 = x2;
            this.y2 = y2;       
        }
        
        /// <summary>
        /// input x-coordinates for the rectangle
        /// </summary>
        /// <param name="x3"></param>
        public void X3(int x3)
        {
            this.x3 = x3;
        }

        /// <summary>
        /// input y-coordinates for the rectangle
        /// </summary>
        /// <param name="y3"></param>
        public void Y3(int y3)
        {
            this.y3 = y3;
        }

        /// <summary>
        /// input radius of the circle
        /// </summary>
        /// <param name="x4"></param>
        public void X4(int x4)
        {
            this.x4 = x4;
        }

        /// <summary>
        /// receives parameters (int x, int y) and draws a rectangle
        /// </summary>
        /// <param name="pictureBox">draw a rectangle</param>
        public void drawRectangle(PictureBox pictureBox)
        {
            Rectangle rectangle = new Rectangle();
            rectangle.drawShape(pictureBox, x1, y1, x3, y3, bitmap);
        }

        /// <summary>
        /// receives parameters (int x, int y) and draws a circle
        /// </summary>
        /// <param name="pictureBox">draws a circle</param>
        public void drawCircle(PictureBox pictureBox)
        {
            Circle circle = new Circle();
            circle.drawShape(pictureBox, x1, y1, x4, bitmap);
        }

        /// <summary>
        /// receives parameters (side, side, side) and draws a triangle
        /// </summary>
        /// <param name="pictureBox">draws a triangle</param>
        public void drawTriangle(PictureBox pictureBox)
        {
            Triangle triangle = new Triangle();
            triangle.drawShape(pictureBox, a, b, c, x1, y1, bitmap);
        }

        /// <summary>
        /// clears the PictureBox of drawings
        /// </summary>
        /// <param name="pictureBox">clears pictureBox</param>
        public void clear(PictureBox pictureBox)
        {
            Graphics graphics = Graphics.FromImage(bitmap);
            graphics.Clear(Color.Transparent);
            pictureBox.Image = bitmap;
        }

        /// <summary>
        /// moves pen to initial position at the top left of the screen
        /// </summary>
        public void reset()
        {
            this.x1 = 0;
            this.y1 = 0;
        }

        /// <summary>
        /// draw line from (x1,y1) position to (x2,y2) position
        /// </summary>
        /// <param name="pictureBox"></param>
        public void drawTo(PictureBox pictureBox)
        {
            Pen p = new Pen(Color.Black, 2);
            Graphics graphics = Graphics.FromImage(bitmap);
            graphics.DrawLine(p, this.x1, this.y1, this.x2, this.y2);
            pictureBox.Image = bitmap;
            p.Dispose();
        }
    }
}
