﻿using System;
using Babalola_Akinloluwa_Samuel___77206952;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject3
{
    [TestClass]
    public class UnitTestCode
    {
        [TestMethod]
        public void Arithmetic_Operators()
        {
            TextCommands text = new TextCommands();
            Assert.AreEqual(15, text.ArithmeticOperator(10, "+", 5));
            Assert.AreEqual(20, text.ArithmeticOperator(27, "-", 7));
            Assert.AreEqual(15, text.ArithmeticOperator(3, "*", 5));
            Assert.AreEqual(8, text.ArithmeticOperator(24, "/", 3));
        }

        [TestMethod]
        public void DrawFunction_Constructed_X1is0()
        {
            DrawFunction draw = new DrawFunction();
            Assert.AreEqual(0, draw.x1);

        }

        [TestMethod]
        public void DrawFunction_Constructed_Y1is0()
        {
            DrawFunction draw = new DrawFunction();
            Assert.AreEqual(0, draw.y1);
        }
        [TestMethod]
        public void Comparison()
        {
            TextCommands text = new TextCommands();
            Assert.AreEqual(true, text.executeIFCondition(10, ">", 5));
            Assert.AreEqual(true, text.executeIFCondition(10, "<", 15));
            Assert.AreEqual(true, text.executeIFCondition(10, "==", 10));
            Assert.AreEqual(true, text.executeIFCondition(10, ">=", 5));
            Assert.AreEqual(true, text.executeIFCondition(10, "<=", 25));
        }
    }
}
